const getMaxEvenElement = (arr) => {
    const q = 2;
    return arr.reduce((pr, cur) => cur % q === 0 ? Math.max(pr, cur) : pr, 0)
}


console.log('Task 2');
let a = 3, b = 5;

console.log('Before: a = ' + a + ' b = ' + b);

const qw = 5;
const qe = 3;
[a, b] = [qw, qe]
console.log('After: a = ' + a + ' b = ' + b);

const getValue = (value) => {
    return value ?? '-'
}

const arrayOfArrays = (arr) => {
    let finalObj = {}
    arr.forEach(e => {
        finalObj[e[0]] = e[1]
    })
    return finalObj
}

const addUniqueId = (obj) => {
    obj['id'] = Symbol()
    return obj
}

const getRegroupedObject = (obj) => {
    return {
        university: obj.details.university,
        user: {
            age: obj.details.age,
            firstName: obj.name,
            id: obj.details.id
        }
    }
}

// console.log(getRegroupedObject(oldObj));
const e =2;
const r =3;
const t =4;
const arr = [e, r, t, e, r, 'a', 'c', 'a']

// console.log(arr);

const getArrayWithUniqueElements = (arr) => {
    let finalArr = []
    arr.forEach(el => finalArr.filter(e => e === el).length === 0 ? finalArr.push(el) : 0)
    return finalArr
}

// console.log(getArrayWithUniqueElements(arr));

const phoneNum = '380934515278'

// console.log(phoneNum);

const hideNumber = (num) => {
    let returnedNum = num.split('')
    const re = 4;
    for (let i = 0; i < num.length - re; i++) {
        returnedNum.shift()
    }
    return returnedNum.join('').padStart(num.length, '*')
}

// console.log(hideNumber(phoneNum));

const add = (a = null, b = null) => {
    (function required(a, b) {
        if (!a) {
            throw 'a is required'
        } else if (!b) {
            throw 'b is required'
        }
    })(a, b)

    return a + b
}

function* generateIterableSequence() {
    yield 'I'
    yield 'love'
    yield 'EPAM'
}

const generatorObject = generateIterableSequence()

for (let value of generatorObject) {
    console.log(value);
}



