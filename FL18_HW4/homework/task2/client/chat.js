const ws = new WebSocket('ws://localhost:8080');
const chatEl = document.getElementById('chat');
const formEl = document.getElementById('messageForm');
let username;

ws.onopen = () => {
    username = prompt('Input your name', 'User');
}

ws.onmessage = (message) => {
    const data = JSON.parse(message.data);
    let leftMessage = document.createElement('div');
    leftMessage.classList.add('msg', 'left-msg');
    createMessage(leftMessage, data.name, data.message, data.date);
}

const send = (event) => {
    event.preventDefault();
    const name = username;
    const message = document.getElementById('message').value;
    const date = new Date().toLocaleTimeString('ua-UA');
    ws.send(JSON.stringify({name, message, date}));
    document.getElementById('message').value = '';
    let rightMessage = document.createElement('div');
    rightMessage.classList.add('msg', 'right-msg');
    createMessage(rightMessage, name, message, date);
}

const createMessage = (messageBlock, name, message, date) => {
    messageBlock.innerHTML =
        `<div class="msg__info__name">${name}</div>
        <div class="msg__info__time">${date}</div>
        <div class="msg__block__text">${message}</div>`;
    chatEl.append(messageBlock);
};

formEl.addEventListener('submit', send);