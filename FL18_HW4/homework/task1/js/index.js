// Your code goes here
async function getUsers(url){
    let response = await fetch(url)
    const data = await response.json()
        
        return data
}


function renderAllUsers (){
 getUsers('https://jsonplaceholder.typicode.com/users')
   .then((data) => {
       data.forEach(user => {
           const { name, username, phone, website } = user
           createUsreCard(name, username, phone, website)
       });
   })
   .catch((err) => console.log(err))
 }
 renderAllUsers();

 function createUsreCard(name, username, phone, website){
     const fragment = document.createDocumentFragment()
     let container = document.querySelector('.container')
     let div = document.createElement('div')
     let p1 = document.createElement('p')
     let p2 = document.createElement('p')
     let p3 = document.createElement('p')
     let p4 = document.createElement('p')
     p1.textContent = `Name: ${name}`
     p2.textContent = `User Name: ${username}`
     p3.textContent = `Phone: ${phone}`
     p4.textContent = `Website: ${website}`
     div.appendChild(p1)
     div.appendChild(p2)
     div.appendChild(p3)
     div.appendChild(p4)
     div.classList.add('form')
     fragment.appendChild(div)
     container.appendChild(fragment)
     let editBut = document.createElement('button')
     let deleteBut =document.createElement('button')
     editBut.textContent = Edit
     deleteBut.textContent = Dlete
     div.appendChild(editBut)
     div.appendChild(deleteBut)

 }